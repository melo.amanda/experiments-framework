<?php

require 'functions.php';

$is_allowed = isset($_GET['idade']) ? checkMinimalAge($_GET['idade']) : false;

$data =[
    "is_allowed" => $is_allowed
];

$task =[
    'title' => 'homework',
    'due' => 'today',
    'assigned_to' => 'amanda',
    'completed' => true
];

require 'index.view.php';