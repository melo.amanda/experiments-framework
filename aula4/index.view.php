<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My html+php</title>
</head>
        
<body>


   <h1>Task for lesson 8</h1>

   <ul>


            <li>
                <strong>Name: </strong> <?= $task['title']; ?>
            </li>
            <li>
                <strong>Date: </strong> <?= $task['due']; ?>
            </li>
            <li>
                <strong>Person Responsable: </strong> <?= $task['assigned_to']; ?>
            </li>
            <li>
                <strong>Status: </strong> <?= $task['completed'] ? 'Complete' : 'Incomplete' ?>
            </li>
   </ul>


   <?= $data ['is_allowed'] ? 'permitido' : 'nao permitido'?>
</body>
</html>